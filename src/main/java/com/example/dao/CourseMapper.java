package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.model.CourseModel;

@Mapper
public interface CourseMapper {
	
	// course view by id
	@Select("SELECT id_course, nama, sks FROM course WHERE id_course = #{id}")
    CourseModel selectCourse (@Param("id") String id);
	
	// course view all
	@Select("SELECT id_course, nama, sks FROM course")
	List<CourseModel> selectAllCourses ();
}
